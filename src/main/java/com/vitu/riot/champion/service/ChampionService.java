package com.vitu.riot.champion.service;

import com.vitu.riot.champion.domain.Champion;
import com.vitu.riot.champion.repository.ChampionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChampionService {

    private final ChampionRepository championRepository;

    public List<Champion> findAll() {
        log.info("Finding all riot champions");
        return championRepository.findAll();
    }

    public Champion save(Champion champion) {
        log.info("Saving new riot champion: {}", champion);
        return championRepository.save(champion);
    }
}
