package com.vitu.riot.champion.web;

import com.vitu.riot.champion.domain.Champion;
import com.vitu.riot.champion.service.ChampionService;
import com.vitu.riot.champion.shared.util.MetricsUtil;
import com.vitu.riot.champion.web.dto.ChampionDto;
import com.vitu.riot.champion.web.mapper.ChampionMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/champion")
public class ChampionResource {

    private final ChampionService championService;
    private final ChampionMapper championMapper;
    private final MetricsUtil metricsUtil;

    @GetMapping
    public ResponseEntity<List<Champion>> getAll() {
        log.info("Request to get all riot champions");
        return ResponseEntity.ok().body(championService.findAll());
    }

    @PostMapping
    public ResponseEntity<Champion> create(@RequestBody ChampionDto championDto) {
        log.info("Request to create riot champion: {}", championDto);
        metricsUtil.incrementCreateChampionMetric();
        Champion champion = championService.save(championMapper.toEntity(championDto));
        return ResponseEntity.status(HttpStatus.CREATED).body(champion);
    }

}
