package com.vitu.riot.champion.domain;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.*;

import static com.vitu.riot.champion.shared.constants.Constants.COLUMN_SIZE;

@Getter
@Setter
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Champion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;
    @Column(name = "description", length = COLUMN_SIZE)
    private String description;
    private String role;
    private String difficulty;
    private String region;

    @OneToMany(mappedBy = "champion", cascade = {CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH}, orphanRemoval = true)
    private List<Skill> skills = new ArrayList<>();

    @OneToMany(mappedBy = "champion", cascade = {CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH}, orphanRemoval = true)
    private Set<Skin> skins = new LinkedHashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Champion champion = (Champion) o;
        return id != null && Objects.equals(id, champion.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
