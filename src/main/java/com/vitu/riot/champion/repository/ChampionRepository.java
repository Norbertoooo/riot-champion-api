package com.vitu.riot.champion.repository;

import com.vitu.riot.champion.domain.Champion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChampionRepository extends JpaRepository<Champion, Long> {
    Optional<Champion> findByNameIgnoreCase(String name);
}
