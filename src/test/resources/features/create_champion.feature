#language:pt

@Done
Funcionalidade: Criar campeão

  Cenário: Criar novo campeão com sucesso
    Dado que não exista o campeão de nome "Bardo"
    Dado o seguinte campeão:
      | name  | description           | role    | region | difficulty |
      | Bardo | O protetor andarilho  | suporte | Targon | Alta       |
    E com as seguintes skills:
      | name                | description                                                                                                                                                                                                                                                                                                                                                                                      |
      | chamado do viajante | Mipes: Bardo atrai pequenos espíritos que ajudam em seus ataques básicos e causam Dano Mágico adicional. Quando Bardo coleta sinos o suficiente, os mipes também causam dano em uma área e reduzem a velocidade de inimigos atingidos. Sinos: sinos ancestrais aparecem aleatoriamente para Bardo coletar. Eles concedem experiência, Velocidade de Movimento fora de combate e restauram Mana.  |
      | prisão cósmica      | Bardo dispara um projétil que causa Lentidão ao primeiro inimigo atingido, mantendo seu trajeto em seguida. Caso atinja uma parede, atordoará o alvo inicial; caso atinja outro inimigo, ambos serão atordoados.                                                                                                                                                                                 |
    E com as seguintes skins:
      | name                | value   | theme       |
      | Bardo Sabugueiro    | 520     | Sabugueiro  |
      | Bardo Kawaii Café   | 1820    | Kawaii Café |
    Quando for feita a requisição do tipo "POST" para criar um novo campeão
    Então deverá retorna o http status 201
    E corpo de resposta:
      | name  | description           | role    | region | difficulty |
      | Bardo | O protetor andarilho  | suporte | Targon | Alta       |
